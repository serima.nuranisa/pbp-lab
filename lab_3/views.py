from django.shortcuts import redirect, render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    form = FriendForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect('/lab-3')

    context = {'form' : form}
    return render(request, "lab3_form.html", context)

