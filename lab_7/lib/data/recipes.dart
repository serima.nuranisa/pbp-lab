import 'package:lab_7/models/recipe.dart';

final allRecipes = <Recipe>[
  Recipe(
    name: 'Lemon Cake', 
    recipeURL: Uri.dataFromString("https://www.foodnetwork.com/recipes/ina-garten/lemon-cake-recipe-1913110",), 
    difficulty: Difficulty.Medium,
    dateAdded: DateTime(2021, 11, 16)
  ),
  Recipe(
    name: 'Soup', 
    recipeURL: Uri.dataFromString("https://www.instagram.com/"), 
    difficulty: Difficulty.Easy,
    dateAdded: DateTime(2021, 11, 17),
  )
];
