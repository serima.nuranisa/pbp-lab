import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Add extends StatelessWidget {
  static const String title = 'Recipe Manager';

  const Add({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(
          fontFamily: 'Raleway',
          scaffoldBackgroundColor: const Color.fromRGBO(255, 197, 204, 1), 
          appBarTheme: const AppBarTheme(color: Color.fromRGBO(244, 161, 182, 1))
        ),
        home: const AddPage(),
      );
}

class AddPage extends StatefulWidget {
  const AddPage({Key? key}) : super(key: key);

  @override
  _AddPageState createState() => _AddPageState();
  
}

class _AddPageState extends State<AddPage> {
  final nameController = TextEditingController();
  final linkController = TextEditingController();
  final DateFormat formatter = DateFormat.yMMMMd('en_US');

  @override
  void initState() {
    super.initState();

    nameController.addListener(() => setState(() {}));
  }
  
  @override
  Widget build(BuildContext context) => Scaffold(
    body: ListView(
      padding: const EdgeInsets.all(32),
      children: [
        buildRecipeName(),
        const SizedBox(height: 24),
        buildRecipeLink(),
        const SizedBox(height: 24),
        ElevatedButton(
          child: const Text("Submit"),
          onPressed: () {
            print('Name: ${nameController.text}');
            print('Link: ${linkController.text}');
            print('Date Added: ${formatter.format(DateTime.now())}');
          },
        ),
      ],
    ),
  );

  Widget buildRecipeName() => TextField(
    controller: nameController,
    decoration: InputDecoration(
      hintText: 'Soup',
      labelText: 'Recipe Name',
      prefixIcon: const Icon(Icons.food_bank_outlined),
      suffixIcon: nameController.text.isEmpty
        ? Container(width: 0)
        : IconButton(
          icon: const Icon(Icons.close),
          onPressed: () => nameController.clear(),
        ),
      border: const OutlineInputBorder(),
    ),
    keyboardType: TextInputType.name,
    textInputAction: TextInputAction.done,  
  );

  Widget buildRecipeLink() => TextField(
    controller: linkController,
    decoration: InputDecoration(
      hintText: 'https://recipe.com',
      labelText: 'Recipe Link',
      prefixIcon: const Icon(Icons.link),
      suffixIcon: linkController.text.isEmpty
        ? Container(width: 0)
        : IconButton(
          icon: const Icon(Icons.close),
          onPressed: () => linkController.clear(),
        ),
      border: const OutlineInputBorder(),
    ),
    keyboardType: TextInputType.url,
    textInputAction: TextInputAction.done,  
  );
}