// ignore_for_file: constant_identifier_names


enum Difficulty {
  Easy,
  Medium,
  Hard,
}

extension ParseToString on Difficulty {
  String toShortString() {
    return toString().split('.').last;
  }
}

extension ParseURLToString on Uri {
  String toURLString() {
    return toString().split(',').last;
  }
}

class Recipe {
  final String name;
  final Uri recipeURL;
  final Difficulty difficulty;
  final DateTime dateAdded;

  const Recipe({
    required this.name,
    required this.recipeURL,
    required this.difficulty,
    required this.dateAdded,
  });

  Recipe copy({
    String? name,
    Uri? recipeURL,
    Difficulty? difficulty,
  }) =>
      Recipe(
        name: name ?? this.name,
        recipeURL: recipeURL ?? this.recipeURL,
        difficulty: difficulty ?? this.difficulty,
        dateAdded: dateAdded,
      );
}