import 'package:flutter/material.dart';
import 'package:lab_6/models/recipe.dart';
import 'package:lab_6/data/recipes.dart';
import 'package:lab_6/widgets/main_drawer.dart';
import 'package:lab_6/widgets/scrollable_widget.dart';
import 'package:intl/intl.dart';


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  static const String title = 'Recipe Manager';

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(
          fontFamily: 'Raleway',
          scaffoldBackgroundColor: const Color.fromRGBO(255, 197, 204, 1), 
          appBarTheme: const AppBarTheme(color: Color.fromRGBO(244, 161, 182, 1))
        ),
        home: const HomePage(),
      );
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) => Scaffold(
    drawer: const MainDrawer(),
    appBar: AppBar(
      title: const Text("Recipe Manager"),
    ),
    body: ScrollableWidget(child: Center(child: buildDataTable())),
    floatingActionButton: const Icon(Icons.add),
  );

  Widget buildDataTable() {
    final columns = ['Name', 'Difficulty', 'URL','Date Added'];

    return DataTable(
      columns: getColumns(columns), 
      rows: getRows(allRecipes),
    );

  }

  List<DataColumn> getColumns(List<String> columns) {
    return columns.map((String column) {

      return DataColumn(
        label: Text(column),
      );
    }).toList();
  }

  List<DataRow> getRows(List<Recipe> recipes) => recipes.map((Recipe recipe) {
    final DateFormat formatter = DateFormat.yMMMMd('en_US');
    final cells = [
      recipe.name,
      recipe.difficulty.toShortString(),
      recipe.recipeURL.toURLString(),
      formatter.format(recipe.dateAdded),
    ];

    return DataRow(
      cells: Utils.modelBuilder(cells, (index, cell) {
        return DataCell(
          Text('$cell'),
        );
      }),
    );
    }).toList();
}

class Utils {
  static List<T> modelBuilder<M, T>(
          List<M> models, T Function(int index, M model) builder) =>
      models
          .asMap()
          .map<int, T>((index, model) => MapEntry(index, builder(index, model)))
          .values
          .toList();
}