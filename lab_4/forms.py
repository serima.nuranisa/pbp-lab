from typing import Text
from django import forms
from django.forms import widgets
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        widgets = {
            'to' : widgets.TextInput(attrs={'placeholder':'Recipient of this note'}),
            'From' : widgets.TextInput(attrs={'placeholder':'Author of this note'}),
            'title' : widgets.TextInput(attrs={'placeholder':'Title of this note'}),

        }