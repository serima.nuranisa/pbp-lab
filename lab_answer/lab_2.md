# LAB 2 Answer

## 1. Apakah perbedaan antara JSON dan XML?
    
* JSON menstruktur data-datanya dengan dictionary sedangkan XML dengan tags
* JSON hanya mendukung UTF-8 encoding sedangkan XML mendukung berbagai tipe encoding
* JSON mendukung arrays sedangkan XML tidak

## 2. Apakah perbedaan antara HTML dan XML?

* HTML didesign untuk menampilkan data sedangkan XML didesign untuk membawa/mengirim data
* HTML menggunakan pre-defined tags sedangkan tags XML di define penulis
* Tags HTML tidak case sensitive sedangkan tags XML case sensitive
